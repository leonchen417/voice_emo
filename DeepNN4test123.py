import tensorflow as tf
#from tensorflow.examples.tutorials.mnist import input_data
import random as random
import pickle
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import precision_recall_fscore_support
'''
Summery:


'''

with open('features_X.pickle','rb') as f:  # Python 3: open(..., 'rb')
    features = pickle.load(f)

with open('Y_oneHot.pickle','rb') as f:  # Python 3: open(..., 'rb')
    labels = pickle.load(f)

#this is new observed data
with open('testing.pickle','rb') as f:  # Python 3: open(..., 'rb')
    try_features = pickle.load(f)

if len(features) == len(labels):
	x_y_indx = np.arange(len(features))


accept_wavelenght = 22050


random.shuffle(x_y_indx) # random shuffle the index for X and Y


#for every 10 iteration, put the value into another array

features = features[x_y_indx]

labels = labels[x_y_indx]

test_feature = np.empty((0,193))


testindx = random.sample(range(0,len(features)),int(len(features)/10)) # random 10% data to test index
test_features = features[[testindx]]
train_features = np.delete(features,testindx,0) #delete test features from features

test_labels = labels[[testindx]]
train_labels =np.delete(labels,testindx,0) 

training_epochs = 5000
n_dim = train_features.shape[1]
n_classes = 7
n_hidden_units_one = 150 
n_hidden_units_two = 200
sd = 1 / np.sqrt(n_dim)
learning_rate = 0.001

X = tf.placeholder(tf.float32,[None,n_dim])
Y = tf.placeholder(tf.float32,[None,n_classes])

W_1 = tf.Variable(tf.random_normal([n_dim,n_hidden_units_one], mean = 0, stddev=sd))
b_1 = tf.Variable(tf.random_normal([n_hidden_units_one], mean = 0, stddev=sd))
h_1 = tf.nn.tanh(tf.matmul(X,W_1) + b_1)

W_2 = tf.Variable(tf.random_normal([n_hidden_units_one,n_hidden_units_two], mean = 0, stddev=sd))
b_2 = tf.Variable(tf.random_normal([n_hidden_units_two], mean = 0, stddev=sd))
h_2 = tf.nn.sigmoid(tf.matmul(h_1,W_2) + b_2)

W = tf.Variable(tf.random_normal([n_hidden_units_two,n_classes], mean = 0, stddev=sd))
b = tf.Variable(tf.random_normal([n_classes], mean = 0, stddev=sd))
y_ = tf.nn.softmax(tf.matmul(h_2,W) + b)

init = tf.global_variables_initializer()

cost_function = -tf.reduce_sum(Y * tf.log(y_))
optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost_function)

correct_prediction = tf.equal(tf.argmax(y_,1), tf.argmax(Y,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

cost_history = np.empty(shape=[1],dtype=float)
y_true, y_pred = None, None

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for epoch in range(training_epochs):            
        _,cost = sess.run([optimizer,cost_function],feed_dict={X:train_features,Y:train_labels})
        cost_history = np.append(cost_history,cost)
    
    y_pred = sess.run(tf.argmax(y_,1),feed_dict={X: test_features})
    y_true = sess.run(tf.argmax(test_labels,1))
    print('Test accuracy: ',round(sess.run(accuracy, feed_dict={X: test_features, Y: test_labels}) , 3))

    #trying new observed data
    y_try_pred = sess.run(tf.argmax(y_,1),feed_dict={X: try_features})
    print('y_try_pred: ',y_try_pred)
    

fig = plt.figure(figsize=(10,8))
plt.plot(cost_history)
plt.axis([0,training_epochs,0,np.max(cost_history)])
plt.show()

p,r,f,s = precision_recall_fscore_support(y_true, y_pred, average='micro')
print("F-Score:", round(f,3))
