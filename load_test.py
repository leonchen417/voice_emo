import librosa
import os
import matplotlib.pyplot as plt
import numpy as np
import pickle
from random import shuffle



targetUrl  = 'c:/Users/vc9p8/Desktop/sound_test'
file = 'try_this_test.wav'
os.chdir(targetUrl)


#def extract_features(fdr,indx,file,emo2int_Maptup):

features = np.empty((0,193))
read,sample_rate= librosa.load(file)

stft = np.abs(librosa.stft(read))
mfccs = np.mean(librosa.feature.mfcc(y=read, sr=sample_rate, n_mfcc=40).T,axis=0)
chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T,axis=0)
mel = np.mean(librosa.feature.melspectrogram(read, sr=sample_rate).T,axis=0)
contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T,axis=0)
tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(read), sr=sample_rate).T,axis=0)


ext_features =np.hstack([mfccs,chroma,mel,contrast,tonnetz])
features = np.vstack([features,ext_features])

with open(targetUrl+'/testing.pickle', 'wb') as f:  # Python 3: open(..., 'wb')
    pickle.dump(features, f)
#return mfccs,chroma,mel,contrast,tonnetz,fdr2int