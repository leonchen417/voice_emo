import librosa
import os
import matplotlib.pyplot as plt
import numpy as np
import pickle
from random import shuffle


baseUrl = 'your_sound_files_directory' #'c:/Users/vc9p8/Desktop/test123'
saveUrl = 'where_to_save_your_features' #'c:/Users/vc9p8/Desktop/sound_test'
os.chdir(baseUrl)

label_folder = os.listdir(baseUrl)

xfull = []

emo2int_Maptup =('anger','anxiety','boredom','contempt','neutral','sadness','shame') 

def extract_features(fdr,indx,file,emo2int_Maptup):
	read,sample_rate= librosa.load(file)
	fdr2int = emo2int_Maptup.index(fdr)+1
	stft = np.abs(librosa.stft(read))
	mfccs = np.mean(librosa.feature.mfcc(y=read, sr=sample_rate, n_mfcc=40).T,axis=0)
	chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T,axis=0)
	mel = np.mean(librosa.feature.melspectrogram(read, sr=sample_rate).T,axis=0)
	contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T,axis=0)
	tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(read), sr=sample_rate).T,axis=0)
	return mfccs,chroma,mel,contrast,tonnetz,fdr2int

def Int2_1hot(labels):
	num_label = len(labels)
	num_classes = len(np.unique(labels))
	_1hotlabel=np.zeros((num_label,num_classes))
	for indx,eachlabel in enumerate(labels):
		_1hotlabel[indx][int(eachlabel-1)]=1

	#print(np.array(labels)-1)
	return _1hotlabel


features, labels = np.empty((0,193)), np.empty((0,1)) #mfccs.shape,chroma.shape,mel.shape,contrast.shape,tonnetz.shape -> 40,12,128,7,6
for fdr in label_folder:
	subdir = baseUrl+'/'+fdr
	os.chdir(subdir)
	wavfiles_list = os.listdir(subdir)
	x_yPair = np.zeros(shape = (len(wavfiles_list),2))
	for indx,file in enumerate(wavfiles_list):
		mfccs,chroma,mel,contrast,tonnetz,fdr2int = extract_features(fdr,indx,file,emo2int_Maptup)
		ext_features =np.hstack([mfccs,chroma,mel,contrast,tonnetz])
		features = np.vstack([features,ext_features])
		labels =np.vstack([labels,fdr2int])

print(features.shape)
print(labels) #numberic display
#print(mfccs.shape,chroma.shape,mel.shape,contrast.shape,tonnetz.shape)

onehot_label = Int2_1hot(labels)
#print(onehot_label)


with open(saveUrl+'/features_X.pickle', 'wb') as f:  # Python 3: open(..., 'wb')
    pickle.dump(features, f)


with open(saveUrl+'/Y_oneHot.pickle', 'wb') as f:  # Python 3: open(..., 'wb')
    pickle.dump(onehot_label, f)
