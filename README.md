# Vioice_emo loads audio speech data and classify the emotion of the speaker.#

### What is this repository for? ###
This example applied Short-time Fourier transform, MFCC,Chroma,Mel,Contrast and 
Tonnetz as signal-processing features to prepare the data for a Multi-layer Neural Network.
The length of the audio is set up to be 1 second.

### How do I get set up? ###

* Summary of set up
* Default Configuration
### Windows 10 ###
### Python 3.5 ###
### librosa==0.5.1 ###
### numpy==1.13.1+mkl ###
### tensorflow-gpu==1.4.0 ###
### tensorflow-tensorboard==0.4.0rc3 ###

### Who do I talk to? ###

Feel free to reach out to me via direct messaging.